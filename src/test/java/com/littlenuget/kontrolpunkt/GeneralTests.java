package com.littlenuget.kontrolpunkt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.littlenuget.kontrolpunkt.controller.rest.VisitRequestAssembler;
import com.littlenuget.kontrolpunkt.model.*;
import org.junit.Test;
import org.springframework.hateoas.Resource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.HashSet;

import static junit.framework.TestCase.assertTrue;

public class GeneralTests
{
    @Test
    public void testBCryptHash()
    {
        String password = "unit";
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashedPassword = encoder.encode(password);
        System.out.println(hashedPassword + " it matches prev " + encoder.matches(password, hashedPassword));
    }

    @Test
    public void givenBidirectionalRelation_whenSerializing_thenException()
        throws JsonProcessingException
    {
        Superior superior = new Superior(new User("sup1", "sup1", new HashSet<>()), new ArrayList<>());
        Unit unit = new Unit(superior, new User("unit", "unit", new HashSet<>()), 10L);
        VisitRequest visitRequest = new VisitRequest(unit, Status.PENDING, 0.0, 0.0);
        unit.addVisitRequest(visitRequest);

        String value = new ObjectMapper().writeValueAsString(visitRequest);

        assertTrue(value.contains("requestedBy"));
    }

    @Test
    public void givenBidirectionRelation_whenSerializing_thenAssembleCorrectly()
    {
        Superior superior = new Superior(new User("sup1", "sup1", new HashSet<>()), new ArrayList<>());
        Unit unit = new Unit(superior, new User("unit", "unit", new HashSet<>()), 10L);
        VisitRequest visitRequest = new VisitRequest(unit, Status.PENDING, 0.0, 0.0);
        unit.addVisitRequest(visitRequest);

        VisitRequestAssembler visitRequestAssembler = new VisitRequestAssembler();
        Resource<VisitRequest> resource = visitRequestAssembler.toResource(visitRequest);

        System.out.println();
    }
}
