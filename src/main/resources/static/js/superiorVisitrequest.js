function getVisitRequests()
{
    const header = $("meta[name='_csrf_header'").attr("content");
    const token = $("meta[name='_csrf']").attr("content");


    const request = $.ajax({
        url: "http://localhost:9999/rest/visitrequests",
        type: "GET",
        headers: { [header] : token },
        success: function(data) {
            console.log("Printing data");
            console.log(data);

//            for (let i = 0; i < data.length; ++i)
//            {
//                let element = data[i];
            $("#list-visitrequests").empty();

//            let acceptHref = element._links.reject.href;
//            let rejectHref = element._links.accept.href;

            data._embedded.visitRequests.forEach(function(element) {
                $("#list-visitrequests").append(
                    "<div class='card mb-3 "
                        + (element.status === "REJECTED" ? "Afvist" : "")
                        + (element.status === "ACCEPTED" ? "Accepteret" : "")
                        + (element.status === "PENDING" ? "Afventer svar" : "")
                    + "'> "
                    + "<img id='map' class='card-img-top' src='' />"
                    + "<div class='card-body'>"
                    + "<p class='card-text'>"
                        + "<span id='status'>Status: " + element.status + "</span>"
                        + "<br>"
                        + "\nOprettet den " + element.createdAt
                        + "<br>"
                        + "\nEnhedsnummer: " + element.requestedByUnit
                        + "<br>"
                        + "\nBreddegrad: <span id='latitude'>" + element.latitude + "</span>"
                        + "<br>"
                        + "\nLængdegrad: <span id='longitude'>" + element.longitude + "</span>"
                        + "<br>"
                        + "</p>"
                    + "</div>"
                    + "<div class='btn-group h-fill' role='group'>"
                    // if
                    + (element._links.hasOwnProperty("accept") ?
                        // Then
                        ("<button type='button' class='btn btn-secondary' onclick=\"updateVisitRequest(this, '"
                            + element._links.accept.href
                        + "')\">" + "Accepter" + "</button>")
                        // Else
                        : "")
                    // If
                    + (element._links.hasOwnProperty("reject") ?
                        // Then
                        ("<button type='button' class='btn btn-secondary' onclick=\"updateVisitRequest(this, '"
                            + element._links.reject.href
                        + "')\">" + "Afvis" + "</button>")
                        // Else
                        : "")
                    + ("<button type='button' class='btn btn-secondary' onclick='initMap(this)'>Vis på kort</button>")
                    + "</div>"
                );
            })
//            }
        }
    })
    .fail(function() {
        alert("Et eller andet gik galt!");
    });
}


function updateVisitRequest(element, url)
{
    const header = $("meta[name='_csrf_header'").attr("content");
    const token = $("meta[name='_csrf']").attr("content");


    $.ajax({
        url: url,
        method: "PUT",
        headers: { [header] : token },
        success: function(data){
            console.log(element.parentElement.parentElement);
            $(element.parentElement.parentElement)
                .removeClass("pending")
                .addClass(data.status.toLowerCase())
                .css({"background-color" : (data.status === "ACCEPTED" ? "lightgreen" : "lightcoral")})
                .find("#status").html((data.status === "ACCEPTED" ? "Status: ACCEPTED" : "Status: REJECTED"));
            $(element.parentElement).remove();
            console.log(data);
        }
    })
}

function initMap(element)
{
    const mapDiv = element.parentElement.parentElement.querySelector("#map");
    console.log(mapDiv);
    const map = new google.maps.Map(mapDiv, {
        center: {lat: 1.0, lng: 1.0},
        zoom: 8
    });
}
