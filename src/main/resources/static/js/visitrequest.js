const baseUrl = window.location.origin;

// proj4js projections from geographic to UTM zone 32
const source = "+proj=longlat +datum=WGS84 +no_defs";
const dest = "+proj=utm +zone=32 +datum=WGS84 +units=m +no_def";

function getLocation()
{
    // Get geolocation data
    let gps = navigator.geolocation;

    if (gps)
    {
        gps.getCurrentPosition(showLocation, failure);
    }
    else
    {
        alert("GPS er ikke tilgængeligt. Det er anbefalet at bruge Chrome og sige ja når siden spørger om tilladelse til at bruge GPS.");
    }
}

function showLocation(position)
{
    let unitNo = document.getElementById("unitNo");
//    let latitude = document.getElementById("latitude");
//    let longitude = document.getElementById("longitude");
    const pos = document.getElementById("pos");

    const asUTM = proj4(source, dest
        , [parseFloat(position.coords.longitude), parseFloat(position.coords.latitude)]
    );

    console.log("lat:  " + position.coords.latitude);
    console.log("long: " + position.coords.longitude);

    simplifyUtm(asUTM);

    pos.innerHTML = asUTM[0] + " " + asUTM[1];
//    latitude.innerHTML = position.coords.latitudpose;
//    longitude.innerHTML = position.coords.longitude;
}

function simplifyUtm(utmValues)
{
    let easting = ("" + utmValues[0])
                        .split('.')[0];
    easting = easting.substring(1, easting.length - 1);
    let northing = ("" + utmValues[1])
                        .split('.')[0];
    northing = northing.substring(2, northing.length - 1);

    utmValues[0] = easting;
    utmValues[1] = northing;
}

function failure()
{
    alert("Refresh siden og accepter for at bruge appen.");
}

function submitPosition()
{
    let gps = navigator.geolocation;

    if(gps)
    {
        gps.getCurrentPosition(saveLocation, failure);
    }
}

function saveLocation(position)
{
    // Just for functionality sake, the url will be hardcoded. In the future it will be revised and follow proper HATEOAS.
    // Create a new visit request with current location, latitude and longitude.
    const header = $("meta[name='_csrf_header'").attr("content");
    const token = $("meta[name='_csrf']").attr("content");

    const capturedUnit = $("#captured_unit").val();
    console.log(capturedUnit);

    console.log(baseUrl + "/rest/visitrequests/create");
    const request = $.ajax({
        url: baseUrl + "/rest/visitrequests/create",
        data: {
              "latitude"  : position.coords.latitude
            , "longitude" : position.coords.longitude
            , "captured_unit" : capturedUnit
        },
        headers: { [header] : token },
        dataType: "json",
        type: "POST",
    })
    .done(function() {
        alert(`Sendte position afsted til overordnet.`)
    })
    .fail(function() {
        alert("Kunne ikke sende position til overordnet.");
    });

    console.log(request);

    // Wait 5 seconds before able to try again!
    document.getElementById("btn-submit").disabled = true;
    setTimeout(function() { document.getElementById("btn-submit").disabled = false }, 5000);
}


function getVisitRequests()
{
    const header = $("meta[name='_csrf_header'").attr("content");
    const token = $("meta[name='_csrf']").attr("content");

    const request = $.ajax({
        url: baseUrl + "/rest/visitrequests",
        type: "GET",
        headers: { [header] : token },
        success: function(data) {
            console.log("Printing data");
            console.log(data);

            $("#list-visitrequests").empty();

//            let acceptHref = element._links.reject.href;
//            let rejectHref = element._links.accept.href;


            // Beautiful beast
            data._embedded.visitRequests.forEach(function(element) {
                let asUTM = proj4(source, dest
                , [parseFloat(element.longitude), parseFloat(element.latitude)]);
                simplifyUtm(asUTM);
                $("#list-visitrequests").append(
                    "<div class='card mb-3 "
                        + (element.status === "REJECTED" ? "rejected" : "")
                        + (element.status === "ACCEPTED" ? "accepted" : "")
                        + (element.status === "PENDING" ? "pending" : "")
                    + "'> "
                    + "<div id='map' class='card-top' src='' />"
                    + "<div class='card-body'>"
                    + "<p class='card-text'>"
                        + "<span id='status'>Status: "
                            + (element.status === "PENDING" ? "Afventer svar"
                                : (element.status === "ACCEPTED" ? "Accepteret" : "Afvist"))
                        + "</span>"
                        + "<br>"
                        + "Oprettet den: " + moment(element.createdAt).format("DD/MM-YY HH:mm:ss")
                        + "<br>"
                        + "Enhedsnummer: " + element.requestedByUnit
                        + "<br>"
                        + "Position: " + asUTM[0] + " " + asUTM[1]
                        + "<span hidden>\nBreddegrad: <span id='latitude'>" + element.latitude + "</span></span>"
                        + "<br>"
                        + "<span hidden>\nLængdegrad: <span id='longitude'>" + element.longitude + "</span></span>"
                        + "<br>"
                        + "</p>"
                    + "</div>"
                    + "<div class='btn-group h-fill' role='group'>"
                    // if
                    + (element._links.hasOwnProperty("accept") ?
                        // Then
                        ("<button type='button' class='btn btn-secondary' onclick=\"updateVisitRequest(this, '"
                            + element._links.accept.href
                        + "')\">" + "Accepter" + "</button>")
                        // Else
                        : "")
                    // If
                    + (element._links.hasOwnProperty("reject") ?
                        // Then
                        ("<button type='button' class='btn btn-secondary' onclick=\"updateVisitRequest(this, '"
                            + element._links.reject.href
                        + "')\">" + "Afvis" + "</button>")
                        // Else
                        : "")
                    + ("<button type='button' class='btn btn-secondary' onclick='initMap(this)'>Vis på kort</button>")
                    + "</div>"
                );
            })
//            }
        }
    })
    .fail(function() {
        alert("Et eller andet gik galt!");
    });
}

function updateVisitRequest(element, url)
{
    const header = $("meta[name='_csrf_header'").attr("content");
    const token = $("meta[name='_csrf']").attr("content");


    $.ajax({
        url: url,
        method: "PUT",
        headers: { [header] : token },
        success: function(data){
            console.log(element.parentElement.parentElement);
            $(element.parentElement.parentElement)
                .removeClass("pending")
                .addClass(data.status.toLowerCase())
                .css({"background-color" : (data.status === "ACCEPTED" ? "lightgreen" : "lightcoral")})
                .find("#status").html((data.status === "ACCEPTED" ? "Status: Accepteret" : "Status: Afvist"));
            $(element.parentElement).prop("disabled", true);
            console.log(data);
        }
    })
}

function initMap(element)
{
    const parent = element.parentElement.parentElement;
    const mapDiv = parent.querySelector("#map");
    const lat = $(parent).find("#latitude").html();
    const lng = $(parent).find("#longitude").html();

    const latLng = {lat: parseFloat(lat), lng: parseFloat(lng)};

    // Beef up
    $(mapDiv).css({"height": "300px"});

    const map = new google.maps.Map(mapDiv, {
        mapTypeId: "hybrid",
        center: latLng,
        zoom: 13
    });

    const marker = new google.maps.Marker({
        position: latLng,
        map: map
    });
}
