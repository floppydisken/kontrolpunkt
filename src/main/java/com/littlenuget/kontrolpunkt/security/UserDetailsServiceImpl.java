package com.littlenuget.kontrolpunkt.security;

import com.littlenuget.kontrolpunkt.persistence.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import com.littlenuget.kontrolpunkt.model.User;

@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
    @Autowired
    UserRepo userRepo;

    @Override
//    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user = userRepo.findByUsername(username);

        return user;
    }
}
