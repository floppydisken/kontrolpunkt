package com.littlenuget.kontrolpunkt.security;

import com.littlenuget.kontrolpunkt.model.User;
import com.littlenuget.kontrolpunkt.persistence.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//@Service
public class UserServiceImpl implements IUserService
{
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserServiceImpl() { }

    @Override
    public void save(User user)
    {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepo.save(user);
    }

    @Override
    public void delete(User user)
    {
        userRepo.delete(user);
    }

    @Override
    public User findByUsername(String username)
    {
        return userRepo.findByUsername(username);
    }
}
