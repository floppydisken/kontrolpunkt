package com.littlenuget.kontrolpunkt.security;

import com.littlenuget.kontrolpunkt.model.User;
import org.springframework.security.core.userdetails.UserDetails;

public interface IUserService
{
    void save(User user);
    void delete(User user);
    User findByUsername(String username);
}
