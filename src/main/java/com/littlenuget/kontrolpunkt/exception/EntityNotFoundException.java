package com.littlenuget.kontrolpunkt.exception;

import org.slf4j.LoggerFactory;

public class EntityNotFoundException extends RuntimeException
{

	public EntityNotFoundException(Class class_, Long id)
	{
		super(String.format("Couldn't find %s with id %d.", class_.toString(), id));
	}
	
}
