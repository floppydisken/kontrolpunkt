package com.littlenuget.kontrolpunkt.persistence;

import java.util.List;

import com.littlenuget.kontrolpunkt.model.Unit;
import com.littlenuget.kontrolpunkt.resources.StringValues;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.littlenuget.kontrolpunkt.model.Status;
import com.littlenuget.kontrolpunkt.model.VisitRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VisitRequestRepo extends JpaRepository<VisitRequest, Long>
{
	List<VisitRequest> findByStatus(Status status);

	List<VisitRequest> findByRequestedBy(Unit unit);
	List<VisitRequest> findByRequestedBy(Unit unit, Sort sort);
}
