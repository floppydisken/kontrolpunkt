package com.littlenuget.kontrolpunkt.persistence;

import com.littlenuget.kontrolpunkt.model.Superior;
import com.littlenuget.kontrolpunkt.model.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperiorRepo extends JpaRepository<Superior, Long>
{
    Superior findByUnits(Unit unit);
}
