package com.littlenuget.kontrolpunkt.persistence;

import com.littlenuget.kontrolpunkt.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepo extends JpaRepository<Authority, Long>
{
    Authority findByAuthority(String authority);
}
