package com.littlenuget.kontrolpunkt.persistence;

import java.util.List;
import java.util.Optional;

import com.littlenuget.kontrolpunkt.model.Superior;
import com.littlenuget.kontrolpunkt.model.Unit;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UnitRepo extends JpaRepository<Unit, Long>
{
	List<Unit> findBySuperior(Superior superior);
}
