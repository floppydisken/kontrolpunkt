package com.littlenuget.kontrolpunkt.controller.view;

import com.littlenuget.kontrolpunkt.controller.BaseController;
import com.littlenuget.kontrolpunkt.model.VisitRequest;
import com.littlenuget.kontrolpunkt.resources.StringValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping({ "/visitrequests", "/visitrequests/"})
public class VisitRequestController extends BaseController
{
    private final static Logger log = LoggerFactory.getLogger(VisitRequestController.class);

    @GetMapping
    public String home(Model model)
    {
        model.addAttribute("visitRequest", new VisitRequest());
        return "index";
    }

    @PostMapping("/create")
    public String addVisitRequest(
        @ModelAttribute("visitRequest") VisitRequest visitRequest
    )
    {

        log.info(String.format("POST for addVisitRequest with variables"
            + "\nstatus: %s"
            + "\nlatitude: %f"
            + "\nlongitude: %f"
            , visitRequest.getStatus()
            , visitRequest.getLatitude()
            , visitRequest.getLongitude()
            )
        );

        return "redirect:visitrequests";
    }
}
