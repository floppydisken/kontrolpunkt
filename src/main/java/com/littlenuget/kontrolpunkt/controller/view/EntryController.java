package com.littlenuget.kontrolpunkt.controller.view;

import com.littlenuget.kontrolpunkt.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Controller
public class EntryController extends BaseController
{
//    private static final String API_KEY = System.getenv("GOOGLE_API_KEY");
    private static final Logger log = LoggerFactory.getLogger(EntryController.class);


    @GetMapping("/error")
    public ResponseEntity error()
    {
        return ResponseEntity.badRequest().build();
    }

    /**
     * If a Unit is logged in we want him routed somewhere else than the Superior
     */
    @GetMapping("/")
    public String routeToRelevantPage(Model model)
    {
        String template = "error";

        if (unit().isPresent())
        {
            model.addAttribute("unitNo", unit().get().getUnitNumber());
            template = "unit_visitrequests";
        }
        else if (superior().isPresent())
        {
            template = "superior_visitrequests";
        }

        return template;
    }
}
