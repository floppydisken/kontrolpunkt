package com.littlenuget.kontrolpunkt.controller.rest;

import com.littlenuget.kontrolpunkt.model.Unit;
import com.littlenuget.kontrolpunkt.resources.StringValues;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.stereotype.Component;

@Component
public class UnitAssembler implements ResourceAssembler<Unit, Resource<Unit>>
{

	@Override
	public Resource<Unit> toResource(Unit unit)
	{
		return new Resource<Unit>(
				  unit
//				, linkTo(methodOn(VisitRequestRestController.class).findPending(unit.getUnitNumber()))
//					.withRel(StringValues.PENDING + "Requests")
//                , linkTo(methodOn(VisitRequestRestController.class).findAccepted(unit.getUnitNumber()))
//                    .withRel(StringValues.ACCEPTED + "Requests")
//				, linkTo(methodOn(VisitRequestRestController.class).findRejected(unit.getUnitNumber()))
//                    .withRel(StringValues.REJECTED + "Requests")
				, linkTo(methodOn(UnitRestController.class).findById(unit.getId()))
					.withSelfRel()
				, linkTo(methodOn(UnitRestController.class).all())
                    .withRel(StringValues.UNITS)
		);
	}
}
