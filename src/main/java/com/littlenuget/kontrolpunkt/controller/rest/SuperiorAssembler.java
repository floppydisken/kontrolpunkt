package com.littlenuget.kontrolpunkt.controller.rest;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.littlenuget.kontrolpunkt.model.Superior;
import org.springframework.stereotype.Component;


@Component
public class SuperiorAssembler implements ResourceAssembler<Superior, Resource<Superior>>
{

	@Override
	public Resource<Superior> toResource(Superior superior)
	{
		return new Resource<>(superior
				, linkTo(methodOn(SuperiorRestController.class).one(superior.getId())).withSelfRel()
//				, linkTo(methodOn(SuperiorRestController.class).all()).withRel("NAME THIS"))
		);
	}

}
