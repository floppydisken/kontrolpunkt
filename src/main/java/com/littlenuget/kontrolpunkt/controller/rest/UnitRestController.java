package com.littlenuget.kontrolpunkt.controller.rest;

import java.util.List;
import java.util.stream.Collectors;

import com.littlenuget.kontrolpunkt.model.Unit;
import com.littlenuget.kontrolpunkt.resources.StringValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.littlenuget.kontrolpunkt.exception.EntityNotFoundException;
import com.littlenuget.kontrolpunkt.persistence.UnitRepo;

@RestController
@RequestMapping(path = { "/rest/units", "/rest/units/" })
public class UnitRestController
{
	@Autowired
	private final UnitRepo uRepo;
	@Autowired
	private final UnitAssembler uAssembler;
	@Autowired
	private final SuperiorRestController sCtrl;

	public UnitRestController(
			  UnitRepo uRepo
			, UnitAssembler uAssembler
			, SuperiorRestController sCtrl
	)
	{
		this.uRepo = uRepo;
		this.uAssembler = uAssembler;
		this.sCtrl = sCtrl;
	}
	
	@GetMapping(produces = StringValues.MEDIA_TYPE)
	public Resources<Resource<Unit>> all()
	{
		List<Resource<Unit>> patrolUnits = uRepo.findAll().stream()
				.map(uAssembler::toResource)
				.collect(Collectors.toList());
		
		return new Resources<>(patrolUnits
				, linkTo(methodOn(UnitRestController.class).all()).withSelfRel());
	}

//	@GetMapping(params = {
//				  StringValues.RequestParams.UNIT_NO
//				, StringValues.RequestParams.SUPERIOR_ID
//            }
//			, produces = StringValues.MEDIA_TYPE
//	)
//	public Resource<Unit> findByUnitNoAndOverseer(
//			  @RequestParam Long unitNo
//			, @RequestParam Long superiorId
//	)
//	{
//		Unit unit = uRepo.findByUnitNumberAndPatrolOverseer(unitNo, superiorId)
//						.orElseThrow(() -> new EntityNotFoundException(Unit.class, unitNo));
//
//		return uAssembler.toResource(unit);
//	}

	@GetMapping(path = { "/{id}", "/{id}/" }
			, produces = StringValues.MEDIA_TYPE
	)
	public Resource<Unit> findById(@PathVariable Long id)
	{
		Unit unit = uRepo.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(Unit.class, id));
		
		return uAssembler.toResource(unit);
	}
}
