package com.littlenuget.kontrolpunkt.controller.rest;

import com.littlenuget.kontrolpunkt.resources.StringValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.littlenuget.kontrolpunkt.model.Status;
import com.littlenuget.kontrolpunkt.model.VisitRequest;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@Component
public class VisitRequestAssembler implements ResourceAssembler<VisitRequest, Resource<VisitRequest>>
{
    private static final Logger log = LoggerFactory.getLogger(VisitRequestAssembler.class);

	@Override
	public Resource<VisitRequest> toResource(VisitRequest visitRequest)
	{
		Resource<VisitRequest> vrResource = new Resource<>(
				  visitRequest
//				, linkTo(methodOn(VisitRequestRestController.class).one(
//                     visitRequest.getId())
//                )
//                    .withSelfRel()
				, linkTo(methodOn(VisitRequestRestController.class).all())
                    .withRel(StringValues.ALL)
            	, linkTo(methodOn(UnitRestController.class).findById(visitRequest.getRequestedBy().getId()))
					.withRel("requestedBy")
//				, linkTo(methodOn(VisitRequestRestController.class).create(visitRequest.getLatitude(), visitRequest.getLongitude()))
//					.withRel("create")
//				, linkTo(methodOn(VisitRequestRestController.class).findAccepted())
//                    .withRel(StringValues.ACCEPTED)
//				, linkTo(methodOn(VisitRequestRestController.class).findRejected())
//                    .withRel(StringValues.REJECTED)
		);

	    if (visitRequest.getStatus() == Status.PENDING)
	    {
	    	vrResource.add(
	    			linkTo(methodOn(VisitRequestRestController.class).accept(visitRequest.getId()))
                        .withRel(StringValues.ACCEPT)
			);
	    	vrResource.add(
	    			linkTo(methodOn(VisitRequestRestController.class).reject(visitRequest.getId()))
                        .withRel(StringValues.REJECT)
			);
	    }

	    return vrResource;
	}
}
