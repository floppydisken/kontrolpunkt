package com.littlenuget.kontrolpunkt.controller.rest;

import com.littlenuget.kontrolpunkt.model.Superior;
import com.littlenuget.kontrolpunkt.resources.StringValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.littlenuget.kontrolpunkt.persistence.SuperiorRepo;
import com.littlenuget.kontrolpunkt.exception.EntityNotFoundException;

@RestController
@RequestMapping("/rest/superiors")
public class SuperiorRestController
{
	@Autowired
	private final SuperiorRepo sRepo;
	private final SuperiorAssembler poAssembler;

	public SuperiorRestController(SuperiorRepo sRepo, SuperiorAssembler poAssembler)
	{
		this.sRepo = sRepo;
		this.poAssembler = poAssembler;
	}
	
	@GetMapping(path = "/{id}", produces = StringValues.MEDIA_TYPE)
	public Resource<Superior> one(Long id)
	{
		Superior superior = sRepo.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(Superior.class, id));

		return poAssembler.toResource(superior);
	}
}
