package com.littlenuget.kontrolpunkt.controller.rest;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.littlenuget.kontrolpunkt.controller.BaseController;
import com.littlenuget.kontrolpunkt.model.*;
import com.littlenuget.kontrolpunkt.persistence.SuperiorRepo;
import com.littlenuget.kontrolpunkt.resources.StringValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import com.littlenuget.kontrolpunkt.persistence.UnitRepo;
import com.littlenuget.kontrolpunkt.persistence.VisitRequestRepo;
import com.littlenuget.kontrolpunkt.exception.EntityNotFoundException;

import static com.littlenuget.kontrolpunkt.model.VisitRequest.Columns.CAPTURED_UNIT;

@RestController
@RequestMapping("/rest/visitrequests")
public class VisitRequestRestController extends BaseController
{
	private final Logger log = LoggerFactory.getLogger(VisitRequestRestController.class);

	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";
	public static final String CAPTURED_UNIT = "captured_unit";

	private Authentication authentication;

	@Autowired
	private final VisitRequestRepo visitRequestRepo;
	@Autowired
	private final VisitRequestAssembler visitRequestAssembler;
	@Autowired
	private final UnitRepo unitRepo;
	@Autowired
	private final UnitRestController unitRestCtrl;
	@Autowired
    private final SuperiorRepo superiorRepo;

	public VisitRequestRestController(
		VisitRequestRepo visitRequestRepo
			, VisitRequestAssembler visitRequestAssembler
			, UnitRepo unitRepo, UnitRestController unitRestCtrl
			, SuperiorRepo superiorRepo
	)
	{
		this.visitRequestRepo = visitRequestRepo;
		this.visitRequestAssembler = visitRequestAssembler;
		this.unitRepo = unitRepo;
		this.unitRestCtrl = unitRestCtrl;
		this.superiorRepo = superiorRepo;
	}

	/**
	 * Find all visit requests based on the currently logged in superior
	 * If a Superior checks in will return all visitrequests made by his subordinates
     * If it's a Unit return own requests
	 * @return
	 */
	@GetMapping(path = "", produces = StringValues.MEDIA_TYPE)
	public ResponseEntity<ResourceSupport> all()
	{
//		return new Resources<>(vrs
//				, linkTo(methodOn(VisitRequestRestController.class).all())
//					.withSelfRel());
        ResponseEntity<ResourceSupport> response = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		if (superior().isPresent())
		{
			List<Unit> units = unitRepo.findBySuperior(superior().get());

			List<VisitRequest> visitRequests = new ArrayList<>();
            units.forEach(
            	unit -> visitRequests.addAll(
            		visitRequestRepo.findByRequestedBy(unit, Sort.by("latitude").descending()))
			);

            // We need to sort again as we don't get proper sorting from fetching
			// requests by unit.
            visitRequests.sort((r1, r2) -> {
            	// This sorting is dependent on the order of the status enum class
                int r1Ord = r1.getStatus().ordinal();
                int r2Ord = r2.getStatus().ordinal();

                return Integer.compare(r1Ord, r2Ord);
			});

			List<Resource<VisitRequest>> vrs = visitRequests.stream()
				.map(visitRequestAssembler::toResource)
				.collect(Collectors.toList());
			response = ResponseEntity.ok(new Resources<>(vrs));


		}
		else if (unit().isPresent())
		{
			List<VisitRequest> visitRequests = visitRequestRepo.findByRequestedBy(unit().get());

			List<Resource<VisitRequest>> vrs = visitRequests.stream()
				.map(visitRequestAssembler::toResource)
				.collect(Collectors.toList());
			response = ResponseEntity.ok(new Resources<>(vrs));
		}

		return response;
	}


	@Secured("ROLE_UNIT")
	@PostMapping(path = "/create"
		, params = {
              LATITUDE
            , LONGITUDE
			, CAPTURED_UNIT
        }
        , produces = StringValues.MEDIA_TYPE
    )
	public ResponseEntity<ResourceSupport> create(
		  @RequestParam(LATITUDE) Double latitude
		, @RequestParam(LONGITUDE) Double longitude
		, @RequestParam(CAPTURED_UNIT) Long capturedUnitId
	)
	{
		Unit unit = unit().get();
	    VisitRequest visitRequest = new VisitRequest(unit, capturedUnitId, Status.PENDING, latitude, longitude);

		unit.addVisitRequest(visitRequest);

		unitRepo.save(unit);

		return ResponseEntity.ok(visitRequestAssembler.toResource(visitRequest));
	}

	@Secured("ROLE_SUPERIOR")
	@PutMapping(path = "/{id}/accept", produces = StringValues.MEDIA_TYPE)
	public ResponseEntity<ResourceSupport> accept(@PathVariable Long id)
	{
		VisitRequest visitRequest = visitRequestRepo.findById(id)
			.orElseThrow(() -> new EntityNotFoundException(VisitRequest.class, id));
		Superior superior = superior().get();

		// If visit requests are part of superiors responsibilities
		Unit unit = visitRequest.getRequestedBy();
		Superior otherSuperior = superiorRepo.findByUnits(unit);

		// If he's responsible
		if (superior.getId() == otherSuperior.getId()
			&& visitRequest.getStatus() == Status.PENDING)
		{
		    visitRequest.setStatus(Status.ACCEPTED);
			visitRequest.setLastModified(new Date(System.currentTimeMillis()));
			return ResponseEntity.ok(visitRequestAssembler.toResource(visitRequestRepo.save(visitRequest)));
		}
        else
		{
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
	}

	@Secured("ROLE_SUPERIOR")
	@PutMapping(path = "/{id}/reject", produces = StringValues.MEDIA_TYPE)
	public ResponseEntity<ResourceSupport> reject(@PathVariable Long id)
	{
		VisitRequest visitRequest = visitRequestRepo.findById(id)
			.orElseThrow(() -> new EntityNotFoundException(VisitRequest.class, id));
		Superior superior = superior().get();

		// If visit requests are part of superiors responsibilities
		Unit unit = visitRequest.getRequestedBy();
		Superior otherSuperior = superiorRepo.findByUnits(unit);

		// If he's responsible
		if (superior.getId() == otherSuperior.getId()
			&& visitRequest.getStatus() == Status.PENDING)
		{
			visitRequest.setStatus(Status.REJECTED);
			visitRequest.setLastModified(new Date(System.currentTimeMillis()));
			return ResponseEntity.ok(visitRequestAssembler.toResource(visitRequestRepo.save(visitRequest)));
		}
		else
		{
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		}
	}
}
