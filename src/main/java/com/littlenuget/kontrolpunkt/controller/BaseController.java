package com.littlenuget.kontrolpunkt.controller;

import com.littlenuget.kontrolpunkt.model.Superior;
import com.littlenuget.kontrolpunkt.model.Unit;
import com.littlenuget.kontrolpunkt.model.User;
import com.littlenuget.kontrolpunkt.persistence.SuperiorRepo;
import com.littlenuget.kontrolpunkt.persistence.UnitRepo;
import net.bytebuddy.implementation.bind.annotation.Super;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public abstract class BaseController
{
    @Autowired
    private SuperiorRepo superiorRepo;
    @Autowired
    private UnitRepo unitRepo;

    public User getUser()
    {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public Optional<Superior> superior()
    {
        return superiorRepo.findById(getUser().getId());
    }

    public Optional<Unit> unit()
    {
        return unitRepo.findById(getUser().getId());
    }

}
