package com.littlenuget.kontrolpunkt.resources;

import org.springframework.http.MediaType;

public class StringValues
{
	public static final String ALL = "all";

	public static final String ACCEPT = "accept";
	public static final String ACCEPTED = "accepted";

	public static final String REJECT = "reject";
	public static final String REJECTED = "rejected";

	public static final String PENDING = "pending";

	public static final String MEDIA_TYPE = MediaType.APPLICATION_JSON_UTF8_VALUE;

	public static final String UNITS = "units";

	/**
	 * We are working with a lot of String parameters. To avoid mistakes, this class will keep track
	 * of the values.
	 * This way we avoid errors as much as possible
     * This will also try to establish consitency
	 */
	public class RequestParams
	{
		public static final String UNIT_ID = "unitId";
		public static final String UNIT_NO = "unitNo";
		public static final String SUPERIOR_ID = "superiorId";
	}
}
