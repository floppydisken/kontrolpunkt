package com.littlenuget.kontrolpunkt.model;

import com.littlenuget.kontrolpunkt.resources.StringValues;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name = "superior")
public class Superior extends User implements Serializable
{
	private static final long serialVersionUID = 6142183723168900823L;

	// Is responsible of these unit
	@OneToMany(cascade=CascadeType.ALL
		, targetEntity = Unit.class
		, mappedBy = "superior"
	)
	private List<Unit> units;
	
	// Mandatory default constructor. Otherwise JPA won't work.
	public Superior()
	{
		units = new ArrayList<>();
	}

	public Superior(User user, List<Unit> units)
	{
	    super(user);
		this.units = units;
	}

	public List<Unit> getUnits()
	{
		return new ArrayList<>(units);
	}

	public void setUnits(List<Unit> units)
	{
		this.units = units;
	}
	
	public void addPatrolUnit(Unit unit)
	{
		units.add(unit);
	}

	public void removePatrolUnit(Unit unit)
	{
		units.remove(unit);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Superior superior = (Superior) o;
		return Objects.equals(units, superior.units);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(units);
	}

	@Override
	public String toString()
	{
		return "Superior{" +
				"units=" + units +
				'}';
	}
}
