package com.littlenuget.kontrolpunkt.model;

import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity
@Table(name = "user_table")
@Inheritance(strategy = InheritanceType.JOINED)
public class User implements UserDetails
{
    @Id
    @GeneratedValue
    @JoinColumn(name = "fk_user")
    private Long id;

    @Column(name = "username", unique = true)
    private String username;
    private String password;

//    @OneToMany(targetEntity = Authority.class)
    @ManyToMany(targetEntity = Authority.class, fetch = FetchType.EAGER)
    private Collection<? extends GrantedAuthority> authorities;

    private boolean isAccountNonExpired;
    private boolean isAccountNonLocked;
    private boolean isCredentialsNonExpired;
    private boolean isEnabled;

    public User()
    {
        // Init default values. To use for constructor chaining
        // We want to ensure this is never null.
        authorities = new HashSet<>();
    }

    public User(
          String username, String password, Collection<? extends GrantedAuthority> authorities
        , boolean isAccountNonExpired, boolean isAccountNonLocked, boolean isCredentialsNonExpired
        , boolean isEnabled
    )
    {
        this();
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.isAccountNonExpired = isAccountNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;
    }

    public User(String username, String password, Collection<? extends GrantedAuthority> authorities)
    {
        this(username, password, authorities, true, true, true, true);
    }

    // Copy constructor
    public User(User user)
    {
        this(user.getUsername(), user.getPassword(), user.getAuthorities()
            , user.isAccountNonExpired(), user.isAccountNonExpired()
            , user.isCredentialsNonExpired(), user.isEnabled()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return authorities;
    }

    public Long getId()
    {
        return id;
    }

    @Override
    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled()
    {
        return isEnabled;
    }
}
