package com.littlenuget.kontrolpunkt.model;

import com.littlenuget.kontrolpunkt.resources.StringValues;

import javax.persistence.Enumerated;

public enum Status
{
	PENDING, ACCEPTED, REJECTED
}
