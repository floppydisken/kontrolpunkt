package com.littlenuget.kontrolpunkt.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

// TODO, This is no longer a VisitRequest, instead it's a Capture
@Entity
public class VisitRequest extends BaseModel
{
    public class Columns
	{
		public static final String STATUS = "status";
		public static final String LAT = "latitude";
		public static final String LONG = "longitude";
		public static final String CAPTURED_UNIT = "captured_unit";
		public static final String FK_UNIT_NO = "fk_unit_no";
	}

	@Enumerated(EnumType.STRING)
	@Column(name = Columns.STATUS)
	private Status status;

	// X axis
	@Column(name = Columns.LAT)
	private double latitude;

	// Y axis
	@Column(name = Columns.LONG)
	private double longitude;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModified;

	@ManyToOne
	@JoinColumn(name = Columns.FK_UNIT_NO)
//    @JsonManagedReference
	private Unit requestedBy;

	@Column(name = Columns.CAPTURED_UNIT)
	private long capturedUnitId;
	
	// Default constructor for Hibernate
	public VisitRequest()
	{
		status = Status.PENDING;
	}

	public VisitRequest(Unit requestedBy, long capturedUnitId, Status status, double latitude
		, double longitude, Date createdAt, Date lastModified
	)
	{
		this.requestedBy = requestedBy;
		this.capturedUnitId = capturedUnitId;
		this.status = status;
		this.longitude = longitude;
		this.latitude = latitude;
		this.createdAt =  createdAt;
		this.lastModified = lastModified;
	}

	public VisitRequest(Unit requestedBy, long capturedUnitId, Status status
		, double latitude, double longitude
	)
	{
	    this(requestedBy, capturedUnitId, status, latitude, longitude, new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
	}

	@JsonGetter
	public long getRequestedByUnit()
	{
		return requestedBy.getUnitNumber();
	}

	public Unit getRequestedBy()
	{
		return requestedBy;
	}

	public Status getStatus()
	{
		return status;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public Date getCreatedAt()
	{
		return createdAt;
	}

	public Date getLastModified()
	{
		return lastModified;
	}

	public long getCapturedUnitId()
	{
		return capturedUnitId;
	}

	public void setLastModified(Date lastModified)
	{
		this.lastModified = lastModified;
	}

	@Override
	public String toString()
	{
		return "VisitRequest{" +
			"status=" + status +
			", capturedUnitId="	+ capturedUnitId +
			", latitude=" + latitude +
			", longitude=" + longitude +
			", createdAt=" + createdAt +
			", lastModified=" + lastModified +
			", requestedBy=" + requestedBy.getUnitNumber() +
			'}';
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		VisitRequest that = (VisitRequest) o;
		return Double.compare(that.latitude, latitude) == 0 &&
				Double.compare(that.longitude, longitude) == 0 &&
				status == that.status;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(super.hashCode(), status, latitude, longitude);
	}
}
