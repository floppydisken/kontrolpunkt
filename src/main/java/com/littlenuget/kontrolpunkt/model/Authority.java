package com.littlenuget.kontrolpunkt.model;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Defines access permissions more or less.
 */
@Entity
public class Authority implements GrantedAuthority, Serializable
{
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String authority;
    private String description;

    public Authority()
    {
        // Default value. We don't want a default auth to have any super priviliges without explicitely telling it to.
    }

    public Authority(String authority, String description)
    {
        this.authority = authority;
        this.description = description;
    }

    public Authority(String authority)
    {
        this(authority, "");
    }

    @Override
    public String getAuthority()
    {
        return authority;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public String toString()
    {
        return "Authority{" +
            "id=" + id +
            ", authority='" + authority + '\'' +
            ", description='" + description + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Authority authority1 = (Authority) o;
        return Objects.equals(id, authority1.id) &&
            Objects.equals(authority, authority1.authority) &&
            Objects.equals(description, authority1.description);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, authority, description);
    }
}
