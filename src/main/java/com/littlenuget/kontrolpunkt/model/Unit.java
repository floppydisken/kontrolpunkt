package com.littlenuget.kontrolpunkt.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.littlenuget.kontrolpunkt.model.User;

// TODO Make HunterForce, can capture a list of units, which are just numbers atm.
@Entity
@Table(name = "unit"
    , uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id", "fk_superior" })
	}
)
public class Unit extends User implements Serializable
{
	public class Columns
	{
		public static final String UNIT_NO = "unit_no";
		public static final String FK_SUPERIOR = "fk_superior";
	}

	private static final long serialVersionUID = 1L;

	/*
	 * Serves as a self defined patrol Id
	 * These two field will be unique
	 */
	@Column(name = Columns.UNIT_NO)
	private Long unitNumber;

	@ManyToOne
	@JoinColumn(name = Columns.FK_SUPERIOR)
	private Superior superior;

	/*
	 * Requests made by this unit
	 */
    // Give VisitRequest the responsibility of name its column.
//    @JoinColumn(name = VisitRequest.Columns.FK_UNIT_NO)
    // TODO Not VisitRequests anymore, they're capture invoices instead.
	@NotNull
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "requestedBy")
	private List<VisitRequest> visitRequests;
	
	// Mandatory default constructor. Otherwise JPA won't work.
	public Unit()
	{
		visitRequests = new ArrayList<>();
	}

	public Unit(Superior superior, User user, Long unitNumber)
	{
	    super(user);
	    this.superior = superior;
	    this.unitNumber = unitNumber;
		visitRequests = new ArrayList<>();
	}

	@JsonValue
	public Long getUnitNumber()
	{
		return unitNumber;
	}
	
	public void setUnitNumber(Long unitNumber)
	{
		this.unitNumber = unitNumber;
	}

	public void addVisitRequest(VisitRequest visitRequest)
	{
		visitRequests.add(visitRequest);
	}

	public void removeVisitRequest(VisitRequest visitRequest)
	{
		visitRequests.remove(visitRequest);
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Unit that = (Unit) o;
		return Objects.equals(unitNumber, that.unitNumber) &&
				Objects.equals(visitRequests, that.visitRequests);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(super.hashCode(), unitNumber, visitRequests);
	}
}
