package com.littlenuget.kontrolpunkt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.littlenuget.kontrolpunkt.model.*;
import com.littlenuget.kontrolpunkt.persistence.AuthorityRepo;
import com.littlenuget.kontrolpunkt.persistence.SuperiorRepo;
import com.littlenuget.kontrolpunkt.persistence.UnitRepo;
import com.littlenuget.kontrolpunkt.security.IUserService;
import com.littlenuget.kontrolpunkt.security.WebSecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.littlenuget.kontrolpunkt.persistence.VisitRequestRepo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@EnableJpaRepositories
@SpringBootApplication
//@SpringBootApplication(scanBasePackages = "com.littlenuget.kontrolpunkt.security")
public class KontrolpunktApplication
{
	private static final Logger log = LoggerFactory.getLogger(KontrolpunktApplication.class);
	@Autowired
	private VisitRequestRepo vrRepo;
	@Autowired
	private SuperiorRepo sRepo;
	@Autowired
	private UnitRepo uRepo;
	@Autowired
	private AuthorityRepo aRepo;
	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;
//	@Autowired
//	private IUserService userService;

	public static void main(String[] args) 
	{
		SpringApplication.run(KontrolpunktApplication.class, args);
	}

	/**
	 * Used to test the application with a replication of in memory data.
	 * @return		What to run
	 */
	@Bean
	public CommandLineRunner run()
	{
		return args -> {
			log.info("Do something");
			Authority userAuth = new Authority("UNIT");
			Authority adminAuth = new Authority("SUPERIOR");
			aRepo.save(userAuth);
			aRepo.save(adminAuth);

			List<GrantedAuthority> authAdmin = new ArrayList<>();
			authAdmin.add(aRepo.findByAuthority(adminAuth.getAuthority()));
			Superior superior = new Superior(new User("overordnet1", bCryptPasswordEncoder.encode("overordnet"), authAdmin), new ArrayList<>());

			sRepo.save(superior);

			// Create 10 users.
            // FIXME Remove this. Should be used only for testing. It's gonna be used for first test run.
			for (int i = 0; i < 10; ++i)
			{

				List<GrantedAuthority> auth = new ArrayList<>();
				auth.add(aRepo.findByAuthority(userAuth.getAuthority()));

				Unit unit = new Unit(superior, new User("enhed" + i, bCryptPasswordEncoder.encode("unit"), auth), (long) i);
//				for (int j = 0; j < 10; ++j)
//				{
//					Thread.sleep(100);
//					VisitRequest visitRequest =
//							new VisitRequest(unit, getRandomStatus(), getRandomCoord(), getRandomCoord());
//					unit.addVisitRequest(visitRequest);
//				}

				uRepo.save(unit);

				superior.addPatrolUnit(unit);
			}

		};
	}

	private double getRandomCoord()
	{
		return (new Random().nextDouble() * 50) + 1;
	}

	private Status getRandomStatus()
	{
		Random r = new Random();
		double rNo = r.nextDouble();

		if (rNo > 0.75)
			return Status.ACCEPTED;
		else if (rNo > 0.25)
			return Status.PENDING;
		else
			return Status.REJECTED;
	}
}

